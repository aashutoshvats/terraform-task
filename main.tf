#Create Iam User

module "create_iam_user" {
  source = "./module/create_iam_user"
  iam_users_name = ["Aashu", "vats"]
}

# Create iam group & Attach policy & user to it 

module "create_iam_group__And_Attach_policy_And_user" {
  depends_on = [
    module.create_iam_user,
  ]
  source          = "./module/create_iam_group"

  iam_groups_name = "developers"
  json_files_name = ["Admin_policy.json", "s3_policy.json"]
  iam_users_name = ["Aashu", "vats"]

}

