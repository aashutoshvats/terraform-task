# Create Groups

resource "aws_iam_group" "create_groups" {
 name = var.iam_groups_name
}


# Attach Json File Policies to Groups

resource "aws_iam_group_policy" "Attach_policies_to-groups" {
  depends_on = [resource.aws_iam_group.create_groups]
  group  = var.iam_groups_name
  count  = length(var.json_files_name)
  policy = file("${path.module}/${element(var.json_files_name, count.index)}")
}


# Add users to groups

resource "aws_iam_user_group_membership" "add_users_to_groups" {
  depends_on = [resource.aws_iam_group_policy.Attach_policies_to-groups]
  groups  = [var.iam_groups_name]
  count = length(var.iam_users_name)
  user = element(var.iam_users_name, count.index)
}





